import React, { useEffect, useState, useRef } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Badge from "@mui/material/Badge";
import { styled, alpha } from "@mui/material/styles";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";

function ResponsiveAppBar() {
  const navigate = useNavigate();
  const token = localStorage.getItem("token");
  const [userIsAdmin, setUserIsAdmin] = useState(true);

  const user = localStorage.getItem("userId");



  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);

  const inputRef = useRef();

  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  const StyledBadge = styled(Badge)(({ theme }) => ({
    "& .MuiBadge-badge": {
      right: -3,
      top: 13,
      border: `2px solid ${theme.palette.background.paper}`,
      padding: "0 4px",
    },
  }));

  const [cartCount, setCartCount] = useState(
    <IconButton
      aria-label="cart"
      sx={{ marginLeft: "5px", color: "gray" }}
      component={Link}
      to="/cart"
    >
      <StyledBadge badgeContent={0} color="warning">
        <ShoppingBagOutlinedIcon sx={{ padding: 0 }} />
      </StyledBadge>
    </IconButton>
  );

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      console.log(inputRef.current.value);

      navigate(`/search/${inputRef.current.value}`);
    }
  };
  
  useEffect(() => {
    // fetch("http://localhost:4000/users/userDetails", {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setUserIsAdmin(result.isAdmin);
        setCartCount(
          <IconButton
            aria-label="cart"
            sx={{ marginLeft: "5px", color: "gray" }}
            component={Link}
            to="/cart"
          >
            <StyledBadge badgeContent={result.cart.length} color="warning">
              <ShoppingBagOutlinedIcon sx={{ padding: 0 }} />
            </StyledBadge>
          </IconButton>
        );
      });
  }, [token, userIsAdmin]);

  return (
    <AppBar position="static" color="transparent">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component={Link}
            to="/"
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "black",
              textDecoration: "none",
              "&:hover": {
                backgroundColor: "transparent",
                boxShadow: "none",
                color: "black",
              },
            }}
          >
            Melúmerri
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="black"
            >
              <MenuIcon style={{ color: "black" }} />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
                color: "black",
              }}
            >
              <MenuItem component={Link} to="/">
                <Typography textAlign="center">Home</Typography>
              </MenuItem>
              <MenuItem component={Link} to="/products">
                <Typography textAlign="center">Products</Typography>
              </MenuItem>
              <MenuItem component={Link} to="/faqs">
                <Typography textAlign="center">FAQs</Typography>
              </MenuItem>
              {userIsAdmin ? (
                <MenuItem component={Link} to="/admin">
                  <Typography textAlign="center">ADMIN DASHBOARD</Typography>
                </MenuItem>
              ) : null}
            </Menu>
          </Box>

          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            <Button
              component={Link}
              to="/"
              sx={{
                my: 2,
                color: "black",
                display: "block",
                "&:hover": {
                  backgroundColor: "transparent",
                  boxShadow: "none",
                  color: "black",
                },
              }}
            >
              Home
            </Button>
            <Button
              component={Link}
              to="/products"
              sx={{
                my: 2,
                color: "black",
                display: "block",
                "&:hover": {
                  backgroundColor: "transparent",
                  boxShadow: "none",
                  color: "black",
                },
              }}
            >
              Products
            </Button>
            <Button
              component={Link}
              to="/faqs"
              sx={{
                my: 2,
                color: "black",
                display: "block",
                "&:hover": {
                  backgroundColor: "transparent",
                  boxShadow: "none",
                  color: "black",
                },
              }}
            >
              FAQs
            </Button>
            {userIsAdmin ? (
              <Button
                component={Link}
                to="/admin"
                sx={{
                  my: 2,
                  color: "black",
                  display: "block",
                  "&:hover": {
                    backgroundColor: "transparent",
                    boxShadow: "none",
                    color: "black",
                  },
                }}
              >
                ADMIN DASHBOARD
              </Button>
            ) : null}
          </Box>

          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
              inputRef={inputRef}
              onKeyDown={handleKeyDown}
            />
          </Search>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <PersonOutlineIcon sx={{ fontSize: 30, color: "gray" }} />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <MenuItem component={Link} to="/profile">
                <Typography textAlign="center">Profile</Typography>
              </MenuItem>
              <MenuItem component={Link} to="/cart">
                <Typography textAlign="center">Shopping Bag</Typography>
              </MenuItem>
              {user === null ? (
                <MenuItem component={Link} to="/login">
                  <Typography textAlign="center">Login</Typography>
                </MenuItem>
              ) : (
                <MenuItem component={Link} to="/logout">
                  <Typography textAlign="center">Logout</Typography>
                </MenuItem>
              )}
            </Menu>
          </Box>
          <FavoriteBorderOutlinedIcon
            sx={{ marginLeft: "10px", color: "gray" }}
          />
          {cartCount}
          {/* <IconButton
            aria-label="cart"
            sx={{ marginLeft: "5px", color: "gray" }}
            component={Link}
            to="/cart"
          >
            <StyledBadge badgeContent={cartCount} color="warning">
              <ShoppingBagOutlinedIcon sx={{ padding: 0 }} />
            </StyledBadge>
          </IconButton> */}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;
