import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
  name: "productDetails",
  initialState: { productList: [] },

  reducers: {
    retrieveProducts(state, action) {
      const productDetailsList = action.payload.productData;

      console.log(`productDetails payload: ${productDetailsList}`);
      state.productList = productDetailsList;

      console.log(state.productList);
    },
  },
});

export const productActions = productSlice.actions;

export default productSlice;
