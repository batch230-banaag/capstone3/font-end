import React, { useEffect, useState } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";

function Products() {
  const [productItems, setProductsItems] = useState([]);

  const token = localStorage.getItem("token");

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setProductsItems(result);
      });
  }, [token]);

  // const productListItems = useSelector(
  //   (state) => state.productReducer.productList
  // );

  // const products = productListItems;
  // console.log(products);

  return (
    <Container fluid>
      <Row
        style={{
          backgroundImage:
            "url(https://ion.r2net.com/Images/engagement-rings/2022/N/SEO/new/engagement-rings-new2.jpg)",
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: "93vh",
        }}
      >
        <Col
          lg={5}
          className="text-center d-flex flex-column justify-content-center align-items-center"
          style={{
            height: "93vh",
            color: "white",
          }}
        >
          <h1 style={{ color: "whitesmoke" }} data-aos="fade-in">
            ENGAGEMENT RINGS
          </h1>
          <p className="product-text ">
            Find the perfect engagement rings for women and men at{" "}
            <strong data-aos="fade-in">Melúmerri</strong>.
          </p>
        </Col>
      </Row>
      <Row style={{ height: "20vh", display: "flex", alignItems: "center" }}>
        <p className="text-center" data-aos="fade-in">
          Discover the epitome of elegance and romance with our collection of
          exquisite engagement rings.
        </p>
      </Row>

      <Row>
        {productItems.map((item) => {
          return (
            <Col key={item._id} lg={4} data-aos="fade-in">
              <Link to={`/ring/${item._id}`}>
                <Card>
                  <Card.Img variant="top" src={item.imgSrc} />
                  <Card.Body>
                    <Card.Title>{item.name}</Card.Title>

                    <Card.Text>{`₱${item.price}`}</Card.Text>
                  </Card.Body>
                </Card>
              </Link>
            </Col>
          );
        })}
      </Row>
    </Container>
  );
}

export default Products;
