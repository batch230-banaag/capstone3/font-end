import React from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import {
  FormControl,
  InputLabel,
  InputAdornment,
  IconButton,
  OutlinedInput,
} from "@mui/material";
import Swal from "sweetalert2";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setpassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [isNotRegistered, setIsNotRegistered] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate();
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  // const dispatch = useDispatch();
  // const userDetails = useSelector((state) => state.userReducer.userDetails);

  const user = localStorage.getItem("email");

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        if (result === true) {
          Swal.fire({
            title: "User not found!",
            icon: "error",
            text: "Please try again.",
          });
          setIsNotRegistered(true);
        } else {
          if (typeof result.accessToken !== "undefined") {
            localStorage.setItem("token", result.accessToken);
            localStorage.setItem("email", email);

            retrieveUserDetails(result.accessToken);

            Swal.fire({
              title: "Login Successful",
              icon: "success",
            });
          } else {
            Swal.fire({
              title: "Authentication failed",
              icon: "error",
              text: "Check your login details and try again.",
            });
          }
        }
        setEmail("");
      });
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        localStorage.setItem("userId", result._id);

        if (result.isAdmin === true) {
          navigate("/admin");
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user !== null ? (
    <Navigate to="/" />
  ) : (
    <Container
      style={{ height: "90vh" }}
      className="d-flex justify-content-center align-items-center p-0"
    >
      <Row
        className="d-flex justify-content-center align-items-center m-0 p-0"
        id="login"
        style={{
          height: "50vh",
          boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px;",
        }}
      >
        <Col lg={8}>
          <h1 className="text-center">Login</h1>
          <Form onSubmit={(event) => loginUser(event)}>
            <FormControl fullWidth sx={{ mt: 5 }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Email
              </InputLabel>
              <OutlinedInput
                label="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type={"text"}
              />
            </FormControl>

            <FormControl fullWidth sx={{ mt: 2, mb: 1 }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                value={password}
                onChange={(e) => setpassword(e.target.value)}
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>

            <Row>
              {isActive ? (
                <Button
                  variant="dark"
                  type="submit"
                  id="submitBtn"
                  className="mt-3"
                >
                  Login
                </Button>
              ) : (
                <Button
                  variant="dark"
                  type="submit"
                  id="submitBtn"
                  className="mt-3"
                  disabled
                >
                  Login
                </Button>
              )}
            </Row>
            <Row>
              {isNotRegistered ? (
                <Button
                  variant="light"
                  type="submit"
                  id="submitBtn"
                  className="mt-3"
                  onClick={() => navigate("/register")}
                >
                  Register
                </Button>
              ) : null}
            </Row>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
