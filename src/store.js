import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./reducer/user-slice";
import productSlice from "./reducer/product-slice";

const store = configureStore({
  reducer: {
    userReducer: userSlice.reducer,
    productReducer: productSlice.reducer,
  },
});

export default store;
